package com.company.demoamplicode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class DemoAmplicodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoAmplicodeApplication.class, args);
    }
}
